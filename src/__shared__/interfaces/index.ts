import { createParamDecorator } from '@nestjs/common';
import { Request } from 'express';
import { User } from '../../auth/entities/auth.entity';

export const ReqUser = createParamDecorator((data, req): any => {
  return req.user;
});

export interface JwtPayload {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: number;
}

export interface ReqUser extends Request {
  user: User;
}

export class Data<T> {
  message?: string;
  data?: T;
}
