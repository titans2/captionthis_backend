import { config } from 'dotenv';

config();

export const global = {
  port: process.env.PORT || 5000,
  environment: process.env.NODE_ENV,
};
export const db = {
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: +process.env.DB_PORT || 5432,
  database:
    global.environment === 'test'
      ? process.env.TEST_DB_NAME
      : process.env.DB_NAME,
  ssl: process.env.DATABASE_SSL,
};
export const jwt = {
  secret: process.env.JWT_SECRET,
  expiresIn: process.env.JWT_EXPIRES_IN,
};
export const firebaseConfig = {
  projectId: process.env.FIREBASE_PROJECT_ID,
  clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
  privateKey: process.env.FIREBASE_SERVICE_PRIVATE_KEY,
  databaseUrl: process.env.FIREBASE_DATABASE_URL,
};
