import { INestApplication } from '@nestjs/common';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';

const config = new DocumentBuilder()
  .addBearerAuth({ type: 'http', scheme: 'bearer' })
  .setTitle('CaptionThis API Docs')
  .setDescription('The CaptionThis API documentation')
  .setVersion('1.0.0')
  .addTag('Home', 'Welcome to the CaptionThis API')
  .addTag('Auth', 'Authentication')
  .addTag('Memes', 'Memes Endpoints')
  .addTag('Captions', 'Caption endpoints')
  .addTag('Lits', 'Lit endpoints')
  .build();

const customOptions: SwaggerCustomOptions = {
  swaggerOptions: {
    persistAuthorization: true,
  },
  customSiteTitle: 'CaptionThis API Doc',
};

export function setupDocs(app: INestApplication): void {
  const document = SwaggerModule.createDocument(app, config);
  return SwaggerModule.setup('/', app, document, customOptions);
}
