import * as admin from 'firebase-admin';
import { firebaseConfig } from './env.config';

export const firebaseInit = () => {
  admin.initializeApp({
    credential: admin.credential.cert({
      projectId: firebaseConfig.projectId, // I get no error here
      clientEmail: firebaseConfig.clientEmail, // I get no error here
      privateKey: firebaseConfig.privateKey.replace(/\\n/g, '\n'), // NOW THIS WORKS!!!
    }),
    databaseURL: firebaseConfig.databaseUrl,
  });
};
