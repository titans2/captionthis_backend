import { Inject, Logger, LoggerService } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'tagNumber', async: false })
export class TagNumber implements ValidatorConstraintInterface {
  constructor(@Inject(Logger) private readonly loggerService: LoggerService) {}
  validate(text: string) {
    return text.split(',').length <= 4;
  }

  defaultMessage() {
    return 'Too many tags, Please use 4 or less.';
  }
}
