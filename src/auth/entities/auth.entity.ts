import { PrimaryGeneratedColumn, Column, Entity, OneToMany } from 'typeorm';
import { Caption } from '../../caption/entities/caption.entity';
import { Lit } from '../../lit/entities/lit.entity';
import { Meme } from '../../meme/entities/meme.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', nullable: false })
  firstName: string;

  @Column({ type: 'varchar', nullable: false })
  lastName: string;

  @Column({ type: 'varchar', nullable: false })
  email: string;

  @Column({
    type: 'varchar',
    nullable: false,
    default:
      'https://material.io/archive/guidelines/assets/0B5-3BCtasWxEV2R6bkNDOUxFZ00/style-icons-product-human-best-do1.png',
  })
  picture: string;

  @OneToMany(() => Meme, (meme) => meme.user)
  memes: Meme[];

  @OneToMany(() => Caption, (caption) => caption.user)
  captions: Caption[];

  @OneToMany(() => Lit, (lit) => lit.user)
  lits: Lit[];
}
