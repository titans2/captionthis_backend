import { AuthService } from './auth.service';
import { Controller, Query, Get } from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  /**
   * @Controller Log in with google
   * @param tokenId
   * @returns a signed jwt token
   */
  @Get('google')
  @ApiQuery({ name: 'tokenId', description: 'TokenId from Google' })
  async googleAuth(
    @Query('tokenId') tokenId: string,
  ): Promise<{ data: { token: string } }> {
    return this.authService.authByGoogle(tokenId);
  }
}
