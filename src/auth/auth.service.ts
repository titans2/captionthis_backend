import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/auth.entity';
import admin from 'firebase-admin';
import { JwtPayload } from '../__shared__/interfaces';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    private jwtService: JwtService,
  ) {}

  /**
   * @Service authentication by google
   * @param tokenId Token id from google
   * @returns a signed jwt token
   */
  async authByGoogle(
    tokenId: string,
  ): Promise<{
    data: {
      token: string;
    };
  }> {
    const payload = await admin.auth().verifyIdToken(tokenId);
    const { name, email, picture } = payload;
    const user = await this.findOneUser({ where: { email } });
    if (!user) {
      const newUser = new User();
      newUser.firstName = name.split(' ')[0];
      newUser.lastName = name.split(' ').slice(1).join(' ');
      newUser.email = email;
      newUser.picture = picture;
      await this.userRepo.save(newUser);
      return { data: { token: this.jwtService.sign({ ...newUser }) } };
    }
    this.userRepo.update(user.id, { picture });
    return { data: { token: this.jwtService.sign({ ...user }) } };
  }

  /**
   * @Service Find one user
   * @param options find options
   * @returns a user entity
   */
  async findOneUser(options: any): Promise<User | null> {
    return await this.userRepo.findOne(options);
  }

  /**
   * @Service Find and validate a user by email
   * @param payload
   * @returns a user entity
   */
  async validateUser(payload: JwtPayload): Promise<User> {
    const user = await this.findOneUser({
      where: { email: payload.email },
    });
    if (!user) throw new UnauthorizedException('Invalid token');
    return user;
  }
}
