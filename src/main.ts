import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { urlencoded, json } from 'express';
import * as helmet from 'helmet';
import { WinstonModule } from 'nest-winston';
import { AppModule } from './app.module';
import { global } from './__shared__/config/env.config';
import { firebaseInit } from './__shared__/config/firebase.config';
import loggerConfig from './__shared__/config/logger.config';
import { setupDocs } from './__shared__/config/swagger.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger(loggerConfig),
  });
  const logger = app.get(Logger);
  /** Catch unhandled rejections
   * @param string
   */
  process.on('unhandledRejection', (e) => {
    logger.error(e);
    process.exit(1);
  });
  app.enableCors({ origin: '*' });

  /**
   * Add global prefix '<host>/api/'
   */
  app.setGlobalPrefix('api');

  /**
   * Use global validation pipe
   */
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );
  /**
   * Set up bodyParser and data limit
   */
  app.use(json({ limit: '10mb' }));
  app.use(
    urlencoded({ limit: '10mb', extended: true, parameterLimit: 1000000 }),
  );

  /**
   * Set security headers
   */
  app.use(helmet());
  /**
   * Set up docs
   * @returns void
   */
  setupDocs(app);

  /**
   * Initialize the firebase app
   * @returns void
   */
  firebaseInit();
  /**
   * Start the app
   * @param port
   * @param callback
   */
  await app.listen(global.port, async () =>
    console.log(`Server running on port ${global.port}`),
  );
}
bootstrap();
