import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { MemeService } from '../meme/meme.service';
import { Data } from '../__shared__/interfaces';
import { CreateCaptionDto } from './dto/create-caption.dto';
import { Caption } from './entities/caption.entity';

@Injectable()
export class CaptionService {
  constructor(
    @InjectRepository(Caption) private captionRepo: Repository<Caption>,
    private readonly memeService: MemeService,
  ) {}

  /**
   * @Service create caption
   * @param user current user
   * @param createCaptionDto caption body
   * @param memeId id of the current meme
   * @returns caption entity object
   */
  async create(
    user: any,
    createCaptionDto: CreateCaptionDto,
    memeId: any,
  ): Promise<Data<Caption>> {
    const newCaption = new Caption();
    newCaption.user = user;
    newCaption.meme = memeId;
    newCaption.text = createCaptionDto.text;
    await this.captionRepo.save(newCaption);
    await this.memeService.updateCaptionCount(newCaption.meme);
    return { data: newCaption };
  }

  /**
   * @Service delete a caption
   * @param user current user id
   * @param id caption id
   * @returns success message
   * @throws not found error
   */
  async delete(user: any, id: string): Promise<Data<null>> {
    const caption = await this.findOneCaption(
      { where: { id, user }, relations: ['meme'] },
      'Caption not here or not yours to delete',
    );
    await this.captionRepo.delete(id);
    await this.memeService.updateCaptionCount(caption.meme.id);
    return { message: 'Caption deleted' };
  }

  /**
   * @Service get all captions by a user
   * @param user user id
   * @returns captions array
   */
  async getAllByUser(
    user: any,
  ): Promise<Data<{ count: number; captions: any[] }>> {
    const captions = await this.captionRepo.findAndCount({
      where: { user },
      relations: ['meme'],
      order: { createdAt: 'DESC' },
    });
    return { data: { count: captions[1], captions: captions[0] } };
  }

  /**
   * @Service get all captions on a meme
   * @param meme meme id
   * @returns captions array
   */
  async getAllByMeme(
    meme: any,
    page: number,
  ): Promise<Data<{ pages: number; count: number; captions: any[] }>> {
    const captionsCount = await this.captionRepo.count({
      where: { meme },
    });
    const pages = Math.ceil(captionsCount / 5);
    const captions = await this.captionRepo.findAndCount({
      where: { meme },
      order: { createdAt: 'DESC' },
      relations: ['user'],
      skip: page * 5,
      take: 5,
    });
    return { data: { pages, count: captions[1], captions: captions[0] } };
  }

  /**
   * @Service find one caption
   * @param options find one options
   * @param message message to pass to the error
   * @returns caption entity object
   * @throws not found exception
   */
  async findOneCaption(
    options: FindOneOptions,
    message: string,
  ): Promise<Caption> {
    const caption = await this.captionRepo.findOne(options);
    if (!caption) throw new NotFoundException(message);
    return caption;
  }

  /**
   * @Service update lit count
   * @returns void
   */
  async updateLitCount(captionId: any) {
    const caption = await this.captionRepo.findOne({
      where: { id: captionId },
      relations: ['lits'],
    });
    caption.litCount = caption.lits.length;
    await this.captionRepo.save(caption);
  }
}
