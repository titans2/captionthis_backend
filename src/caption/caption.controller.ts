import {
  Controller,
  Post,
  Body,
  Param,
  ParseUUIDPipe,
  UseGuards,
  Delete,
  Get,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { User } from '../__shared__/decorators/user.decorator';
import { Data, JwtPayload } from '../__shared__/interfaces';
import { CaptionService } from './caption.service';
import { CreateCaptionDto } from './dto/create-caption.dto';
import { Caption } from './entities/caption.entity';

@Controller('captions')
@ApiTags('Captions')
export class CaptionController {
  constructor(private readonly captionService: CaptionService) {}

  /**
   * @Controller create a new caption
   * @param user current user
   * @param createCaptionDto caption details in body
   * @param memeId id of the meme in question
   * @returns new caption entity object
   * @throws unauthorized error
   */
  @Post('meme/:memeId')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Caption created' })
  @ApiUnauthorizedResponse({ description: 'Not logged in' })
  create(
    @User() user: JwtPayload,
    @Body() createCaptionDto: CreateCaptionDto,
    @Param('memeId', ParseUUIDPipe) memeId: string,
  ) {
    return this.captionService.create(user, createCaptionDto, memeId);
  }

  /**
   * @Controller find captions on a meme
   * @param memeId id of the meme in question
   * @returns captions array
   */
  @Get('meme/:memeId')
  @ApiOkResponse({ description: 'Captions retrieved' })
  getAllByMeme(
    @Param('memeId', ParseUUIDPipe) memeId: string,
    @Query('page', ParseIntPipe) page: number,
  ) {
    return this.captionService.getAllByMeme(memeId, page);
  }

  /**
   * @Controller get all captions by a user
   * @param userId user's id
   * @returns captions array
   * @throws unauthorized error
   */
  @Get('user/:userId')
  @ApiCreatedResponse({ description: 'User Captions retrieved' })
  @ApiUnauthorizedResponse({ description: 'Not logged in' })
  getAllByUser(
    @Param('userId', ParseUUIDPipe) userId: string,
  ): Promise<Data<{ count: number; captions: Caption[] }>> {
    return this.captionService.getAllByUser(userId);
  }

  /**
   * @Controller delete a caption
   * @param user current user
   * @param captionId id of the caption in question
   * @returns deleted message
   * @throws unauthorized error
   */
  @Delete(':captionId')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Caption deleted' })
  @ApiUnauthorizedResponse({ description: 'Not logged in' })
  delete(
    @User() user: JwtPayload,
    @Param('captionId', ParseUUIDPipe) captionId: string,
  ): Promise<Data<null>> {
    return this.captionService.delete(user.id, captionId);
  }
}
