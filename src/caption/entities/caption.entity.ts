import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../../auth/entities/auth.entity';
import { Lit } from '../../lit/entities/lit.entity';
import { Meme } from '../../meme/entities/meme.entity';

@Entity('captions')
export class Caption {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  text: string;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @ManyToOne(() => User, (user) => user.captions)
  user: User;

  @ManyToOne(() => Meme, (meme) => meme.captions)
  meme: Meme;

  @OneToMany(() => Lit, (lit) => lit.caption)
  lits: Lit[];

  @Column({ default: 0 })
  litCount: number;
}
