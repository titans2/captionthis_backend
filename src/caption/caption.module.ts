import { Module } from '@nestjs/common';
import { CaptionService } from './caption.service';
import { CaptionController } from './caption.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Caption } from './entities/caption.entity';
import { PassportModule } from '@nestjs/passport';
import { MemeModule } from '../meme/meme.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Caption]),
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
    MemeModule,
  ],
  controllers: [CaptionController],
  providers: [CaptionService],
  exports: [CaptionService],
})
export class CaptionModule {}
