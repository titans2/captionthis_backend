import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';

export class CreateCaptionDto {
  @IsString()
  @MinLength(5, {
    message: 'Caption is too short, must be more that 5 characters',
  })
  @ApiProperty({
    type: 'string',
    description: 'Caption text',
    default: "When you're trying to sleep but you know tomorrow is demo day",
  })
  text: string;
}
