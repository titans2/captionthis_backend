import { Logger, Module } from '@nestjs/common';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import ormconfig from '../ormconfig';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { LoggerInterceptor } from './__shared__/interceptors/logger.interceptor';
import { ResponseInterceptor } from './__shared__/interceptors/res.interceptor';
import { MemeModule } from './meme/meme.module';
import { ExceptionsFilter } from './__shared__/filters/exceptions.filter';
import { CaptionModule } from './caption/caption.module';
import { LitModule } from './lit/lit.module';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';

@Module({
  imports: [
    TypeOrmModule.forRoot(ormconfig),
    ThrottlerModule.forRoot({ ttl: 60, limit: 30 }),
    AuthModule,
    MemeModule,
    CaptionModule,
    LitModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    Logger,
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggerInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: ExceptionsFilter,
    },
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
  ],
  exports: [],
})
export class AppModule {}
