import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../../auth/entities/auth.entity';
import { Caption } from '../../caption/entities/caption.entity';

@Entity('memes')
export class Meme {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  imageSmall: string;

  @Column()
  imageLarge: string;

  @Column()
  tags: string;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @ManyToOne(() => User, (user) => user.memes)
  user: User;

  @OneToMany(() => Caption, (caption) => caption.meme)
  captions: Caption[];

  @Column({ default: 0 })
  captionCount: number;
}
