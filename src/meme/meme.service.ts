import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, ILike, Repository } from 'typeorm';
import { Data } from '../__shared__/interfaces';
import { User } from '../auth/entities/auth.entity';
import { Caption } from '../caption/entities/caption.entity';
import { CreateMemeDto } from './dto/create-meme.dto';
import { Meme } from './entities/meme.entity';

@Injectable()
export class MemeService {
  constructor(
    @InjectRepository(Meme) private memeRepo: Repository<Meme>,
    @InjectRepository(Caption) private captionRepo: Repository<Caption>,
    @InjectRepository(User) private userRepo: Repository<User>,
  ) {}

  /**
   * @Service create meme
   *
   * @param createMemeDto the meme object
   * @param user current user
   * @returns meme entity object
   * @throws unauthorized
   */
  async create(
    createMemeDto: CreateMemeDto,
    user: any,
  ): Promise<{
    data: Meme;
  }> {
    createMemeDto.tags = createMemeDto.tags
      .split(',')
      .map((tag) => ` ${tag} `)
      .join(',');
    const newMeme = new Meme();
    newMeme.tags = createMemeDto.tags;
    newMeme.imageLarge = createMemeDto.imageLarge;
    newMeme.imageSmall = createMemeDto.imageSmall;
    newMeme.user = user;
    await this.memeRepo.save(newMeme);
    return { data: newMeme };
  }

  /**
   * @Service delete a meme
   * @param user current user
   * @param id meme id
   * @returns message
   * @throws Not found error
   */
  async delete(
    user: any,
    id: string,
  ): Promise<{
    message: string;
  }> {
    const meme = await this.findOne({ where: { id, user } }, 'Meme not found');
    await this.memeRepo.delete(meme.id);
    return { message: 'Meme deleted' };
  }

  /**
   * @Service find all memes
   * @param page pagination number
   * @returns memes array
   */
  async findAll(page: number, search?: string): Promise<any> {
    const memesCount = await this.memeRepo.count();
    const pages = Math.ceil(memesCount / 10);
    const memes =
      search !== null && search?.trim() !== ''
        ? await this.memeRepo.find({
            where: {
              tags: ILike(`% ${search}%`),
            },
            relations: ['user'],
            order: { createdAt: 'DESC' },
            skip: page * 10,
            take: 10,
          })
        : await this.memeRepo.find({
            relations: ['user'],
            order: { createdAt: 'DESC' },
            skip: page * 10,
            take: 10,
          });
    return { data: { pages, memes } };
  }

  /**
   * @Service find featured memes
   * @returns memes array
   */
  async findAllFeatured(): Promise<Data<any[]>> {
    const memes = await this.memeRepo.find({
      relations: ['user'],
      order: {
        captionCount: 'DESC',
      },
      take: 10,
    });
    if (
      memes.length > 1 &&
      Math.min(...memes.map((meme) => meme.captionCount)) > 2
    ) {
      const featuredMemes = memes.map(async (meme) => {
        const featuredCaption = await this.captionRepo.find({
          where: { meme },
          order: { litCount: 'DESC' },
        })[0];
        return {
          ...meme,
          featuredCaption,
        };
      });
      return { data: featuredMemes };
    }
    return { data: [] };
  }

  /**
   * @Service find all memes by a user
   * @param options find conditions
   * @returns memes array
   */
  async findAllByUser(userId: any, page: number): Promise<any> {
    const memesCount = await this.memeRepo.count({ where: { user: userId } });
    const pages = Math.ceil(memesCount / 10);
    const memes = await this.memeRepo.find({
      where: { user: userId },
      relations: ['user'],
      order: { createdAt: 'DESC' },
      skip: page * 10,
      take: 10,
    });
    const thisUser = await this.userRepo.findOne(userId);
    return { data: { user: thisUser, pages, memes } };
  }

  /**
   * @Service find memes by tag
   * @param tag tag as search query
   * @returns memes array
   */
  async findByTag(tag: string): Promise<Data<Meme[]>> {
    const memes = await this.memeRepo.find({
      relations: ['user'],
      where: { tags: ILike(`% ${tag}%`) },
      order: { createdAt: 'DESC' },
    });
    return { data: memes };
  }

  /**
   * @Service find user's memes by tag
   * @param tag tag as search query
   * @param user user id
   * @returns memes array
   */
  async findByTagUser(tag: string, user: any): Promise<any> {
    const memes = await this.memeRepo.find({
      relations: ['user'],
      where: { user, tags: ILike(`% ${tag}%`) },
      order: { createdAt: 'DESC' },
    });
    const thisUser = await this.userRepo.findOne(user);
    return { data: { user: thisUser, memes } };
  }

  /**
   * @Service find one meme with captions and lits
   * @param options find conditions
   * @param message message to pass to the error
   * @returns meme object
   * @throws Not found error
   */
  async findOneMeme(memeId: string): Promise<any> {
    const meme = await this.findOne(
      { where: { id: memeId }, relations: ['user'] },
      'Meme not found',
    );
    const captions = await this.captionRepo.find({
      where: { meme: meme.id },
      relations: ['user'],
      order: { createdAt: 'DESC' },
    });

    return {
      ...meme,
      captions,
    };
  }

  /**
   * @Service find one meme
   * @param options find conditions
   * @param message message to pass to the error
   * @returns meme object
   * @throws Not found error
   */
  async findOne(options: FindOneOptions, message: string): Promise<Meme> {
    const meme = await this.memeRepo.findOne(options);
    if (!meme) throw new NotFoundException(message);
    return meme;
  }

  /**
   * @Service update caption count
   * @returns void
   */
  async updateCaptionCount(memeId: any) {
    const meme = await this.memeRepo.findOne({
      where: { id: memeId },
      relations: ['captions'],
    });
    meme.captionCount = meme.captions.length;
    await this.memeRepo.save(meme);
  }
}
