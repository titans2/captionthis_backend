import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Validate } from 'class-validator';
import { TagNumber } from '../../__shared__/utils';

export class CreateMemeDto {
  @ApiProperty({
    type: 'string',
    description: 'Small Image url',
    default:
      'https://static01.nyt.com/images/2020/09/27/fashion/23RECOVERYMEMES-top/oakImage-1600878401377-articleLarge.jpg?quality=75&auto=webp&disable=upscale',
  })
  @IsString()
  @IsNotEmpty()
  imageSmall: string;

  @ApiProperty({
    type: 'string',
    description: 'Large Image url',
    default: 'https://avatars.githubusercontent.com/u/53472419?v=4',
  })
  @IsString()
  @IsNotEmpty()
  imageLarge: string;

  @ApiProperty({
    type: 'string',
    description: 'Tags',
    default: 'Awesomiy, CodeOfAfrica, CaptionThis, Milk',
  })
  @IsNotEmpty()
  @IsString()
  @Validate(TagNumber)
  tags: string;
}
