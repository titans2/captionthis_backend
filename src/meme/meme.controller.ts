import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  ParseUUIDPipe,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { SkipThrottle } from '@nestjs/throttler';
import { User } from '../__shared__/decorators/user.decorator';
import { Data, JwtPayload } from '../__shared__/interfaces';
import { CreateMemeDto } from './dto/create-meme.dto';
import { Meme } from './entities/meme.entity';
import { MemeService } from './meme.service';

@Controller('memes')
@ApiTags('Memes')
export class MemeController {
  constructor(private readonly memeService: MemeService) {}

  /**
   * @Controller create a new meme
   * @param createMemeDto meme details in body
   * @param user current user
   * @returns new meme entity object
   * @throws unauthorized error
   */
  @Post()
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: 'Meme created successfully' })
  @ApiUnauthorizedResponse({ description: 'Not logged in' })
  create(@Body() createMemeDto: CreateMemeDto, @User() user: JwtPayload) {
    return this.memeService.create(createMemeDto, user.id);
  }

  /**
   * @Controller Find all memes
   * @returns memes array
   */
  @Get()
  @SkipThrottle()
  @ApiQuery({ name: 'search', required: false })
  @ApiOkResponse({ description: 'Memes retrieved' })
  findAll(
    @Query('page', ParseIntPipe) page: number,
    @Query('search') search?: string,
  ): Promise<any> {
    return this.memeService.findAll(page, search);
  }

  /**
   * @Controller Find featured memes
   * @returns memes array
   */
  @Get('featured')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Featured Memes retrieved' })
  findAllFeatured(): Promise<Data<Meme[]>> {
    return this.memeService.findAllFeatured();
  }

  /**
   * @Controller Find all memes by a user
   * @param user current user
   * @param scope memes scope: all or just mine
   * @returns memes array
   */
  @Get('user/:userId')
  @SkipThrottle()
  @ApiOkResponse({ description: 'Memes retrieved' })
  findAllByUser(
    @Param('userId', ParseUUIDPipe) userId: string,
    @Query('page', ParseIntPipe) page: number,
  ): Promise<any> {
    return this.memeService.findAllByUser(userId, page);
  }

  /**
   * @Controller Find memes by tag
   * @param user current user
   * @param tag tag as search query
   * @returns memes array
   */
  @Get('search')
  @SkipThrottle()
  @ApiOkResponse({ description: 'Memes retrieved' })
  searchByTag(@Query('tag') tag: string): Promise<Data<Meme[]>> {
    return this.memeService.findByTag(tag);
  }

  /**
   * @Controller Find memes by tag
   * @param user current user
   * @param tag tag as search query
   * @returns memes array
   */
  @Get('search/:userId')
  @SkipThrottle()
  @ApiOkResponse({ description: 'Memes retrieved' })
  searchByTagUser(
    @Query('tag') tag: string,
    @Param('userId', ParseUUIDPipe) userId: string,
  ): Promise<Data<Meme[]>> {
    return this.memeService.findByTagUser(tag, userId);
  }

  /**
   * @Controller find one meme
   * @param memeId meme id
   * @returns meme entity object
   */
  @Get(':memeId')
  @ApiOkResponse({ description: 'Meme retrieved' })
  async findOne(
    @Param('memeId', ParseUUIDPipe) memeId: string,
  ): Promise<Data<Meme>> {
    return {
      data: await this.memeService.findOneMeme(memeId),
    };
  }

  /**
   * @Controller delete a meme
   * @param user current user
   * @returns success message
   */
  @Delete(':memeId')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Meme deleted' })
  delete(
    @User() user: JwtPayload,
    @Param('memeId', ParseUUIDPipe) memeId: string,
  ): Promise<{ message: string }> {
    return this.memeService.delete(user.id, memeId);
  }
}
