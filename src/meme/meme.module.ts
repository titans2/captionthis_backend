import { Module } from '@nestjs/common';
import { MemeService } from './meme.service';
import { MemeController } from './meme.controller';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Meme } from './entities/meme.entity';
import { Caption } from '../caption/entities/caption.entity';
import { User } from '../auth/entities/auth.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Meme, Caption, User]),
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
  ],
  controllers: [MemeController],
  providers: [MemeService],
  exports: [MemeService],
})
export class MemeModule {}
