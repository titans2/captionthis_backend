import { Controller, Get } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@ApiTags('Home')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('welcome')
  @ApiResponse({ status: 200, description: 'Success' })
  getHello() {
    return this.appService.getHello();
  }
}
