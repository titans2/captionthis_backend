import {
  Controller,
  UseGuards,
  Param,
  ParseUUIDPipe,
  Put,
} from '@nestjs/common';
import { LitService } from './lit.service';
import { User } from '../__shared__/decorators/user.decorator';
import { Data, JwtPayload } from '../__shared__/interfaces';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

@Controller('lits')
@UseGuards(AuthGuard())
@ApiBearerAuth()
@ApiTags('Lits')
export class LitController {
  constructor(private readonly litService: LitService) {}

  /**
   * @Controller Lit/Unlit to caption
   * @param user current user
   * @param captionId current caption
   * @returns message
   * @throws unauthorized error
   */
  @Put('caption/:captionId')
  @ApiCreatedResponse({ description: 'Lit added/Removed' })
  @ApiUnauthorizedResponse({ description: 'Not Logged in' })
  create(
    @User() user: JwtPayload,
    @Param('captionId', ParseUUIDPipe) captionId: string,
  ): Promise<Data<any>> {
    return this.litService.lit(user.id, captionId);
  }
}
