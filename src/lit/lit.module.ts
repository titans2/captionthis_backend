import { Module } from '@nestjs/common';
import { LitService } from './lit.service';
import { LitController } from './lit.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lit } from './entities/lit.entity';
import { PassportModule } from '@nestjs/passport';
import { CaptionModule } from '../caption/caption.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Lit]),
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
    CaptionModule,
  ],
  controllers: [LitController],
  providers: [LitService],
})
export class LitModule {}
