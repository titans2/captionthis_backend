import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CaptionService } from '../caption/caption.service';
import { Data } from '../__shared__/interfaces';
import { Lit } from './entities/lit.entity';

@Injectable()
export class LitService {
  constructor(
    @InjectRepository(Lit) private litRepo: Repository<Lit>,
    private readonly captionService: CaptionService,
  ) {}

  /**
   * @Service create lit
   * @returns new lit
   * @throws unauthorized
   */
  async lit(user: any, captionId: any): Promise<Data<any>> {
    const myLit = await this.litRepo.findOne({
      where: { user, caption: captionId },
    });
    if (!myLit) {
      const newLit = new Lit();
      newLit.caption = captionId;
      newLit.user = user;
      await this.litRepo.save(newLit);
      await this.captionService.updateLitCount(newLit.caption);
      return { data: { litAdded: true } };
    }
    await this.litRepo.delete(myLit.id);
    await this.captionService.updateLitCount(captionId);
    return { data: { litRemoved: true } };
  }
}
