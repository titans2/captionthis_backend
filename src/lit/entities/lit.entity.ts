import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../../auth/entities/auth.entity';
import { Caption } from '../../caption/entities/caption.entity';

@Entity('lits')
export class Lit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Caption, (caption) => caption.lits)
  caption: Caption;

  @ManyToOne(() => User, (user) => user.lits)
  user: User;
}
