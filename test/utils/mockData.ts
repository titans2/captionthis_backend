import { User } from '../../src/auth/entities/auth.entity';

export default {
  gitegob7Token:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImIwMTkyZjcwLWYxZTItNDhhYi1iNWI2LTYyMGNhZWM0ZjdkOCIsImZpcnN0TmFtZSI6IkJyaWFuIiwibGFzdE5hbWUiOiJHaXRlZ28iLCJlbWFpbCI6ImdpdGVnb2I3QGdtYWlsLmNvbSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHalpEeU8tbGVXMW5SWkRzMDBjOENKVTd3ZkdkT1ZRU0VBbEprUjV0UT1zOTYtYyIsImlhdCI6MTYxOTY5NzgwMX0.XN5RyE_SRM1usGx3iro5yqAkTGohp9yZY8F0RKikLp4',
  devGitegoToken:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJCcmlhbiIsImxhc3ROYW1lIjoiR2l0ZWdvIiwiZW1haWwiOiJkZXYuZ2l0ZWdvQGdtYWlsLmNvbSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vLUxvZGFVQ21tYXpVL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FNWnV1Y2w2Q1VMVjUzMGNPZ1dkenlSSGQ0Y3EweVpxUHcvczk2LWMvcGhvdG8uanBnIiwiaWQiOiJmMzE5Y2Y0Ni1iYjM2LTQ4YTEtOTg1NS1mZDgyODAzZjg1NDkiLCJpYXQiOjE2MTk2OTc4ODJ9.e23EUASzCMPZqzd-I0jFFlDFkAbfzvU7MWi9TekK7wU',
  newMeme: {
    imageSmall:
      'https://static01.nyt.com/images/2020/09/27/fashion/23RECOVERYMEMES-top/oakImage-1600878401377-articleLarge.jpg?quality=75&auto=webp&disable=upscale',
    imageLarge: 'https://avatars.githubusercontent.com/u/53472419?v=4',
    tags: 'testing,posting,new,meme',
  },
  newMemeMoreTags: {
    imageSmall:
      'https://static01.nyt.com/images/2020/09/27/fashion/23RECOVERYMEMES-top/oakImage-1600878401377-articleLarge.jpg?quality=75&auto=webp&disable=upscale',
    imageLarge: 'https://avatars.githubusercontent.com/u/53472419?v=4',
    tags: 'testing,posting,new,meme,extra',
  },
  newCaption: {
    text: 'Something very cool to that it gets the most lits',
  },
  gitegob7MemeId: '',
  devGitegoMemeId: '',
  devGitegoId: '',
  gitegob7Id: '',
  devGitegoCaptionId: '',
};

export const createSeedUsers = (): User[] => {
  const devGitego = new User();
  devGitego.firstName = 'Dev';
  devGitego.lastName = 'Gitego';
  devGitego.email = 'dev.gitego@gmail.com';
  devGitego.picture =
    'https://static01.nyt.com/images/2020/09/27/fashion/23RECOVERYMEMES-top/oakImage-1600878401377-articleLarge.jpg?quality=75&auto=webp&disable=upscale';
  const gitegob7 = new User();
  gitegob7.firstName = 'Brian';
  gitegob7.lastName = 'Gitego';
  gitegob7.email = 'gitegob7@gmail.com';
  gitegob7.picture =
    'https://static01.nyt.com/images/2020/09/27/fashion/23RECOVERYMEMES-top/oakImage-1600878401377-articleLarge.jpg?quality=75&auto=webp&disable=upscale';
  return [devGitego, gitegob7];
};
