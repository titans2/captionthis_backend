import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Connection, getConnection, getRepository } from 'typeorm';
import { AppModule } from '../src/app.module';
import mockData, { createSeedUsers } from './utils/mockData';
import { User } from '../src/auth/entities/auth.entity';

describe('Memes (e2e)', () => {
  let app: INestApplication;
  let connection: Connection;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        forbidNonWhitelisted: true,
      }),
    );
    await app.init();
    connection = getConnection('default');
    await connection.synchronize(true);
    // SEED USERS
    const userRepo = getRepository(User, 'default');
    await userRepo.save(createSeedUsers());
  });
  afterAll(async () => {
    await connection.synchronize(true);
    await app.close();
  });

  it(`/POST Authenticated User devGitego should be able to post a meme`, async () => {
    const res = await request(app.getHttpServer())
      .post('/memes')
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken)
      .send(mockData.newMeme);
    mockData.devGitegoMemeId = res.body.data.id;
    mockData.devGitegoId = res.body.data.user;
    return expect(res.status).toEqual(201);
  });
  it(`/POST Authenticated User devGitego should be not be able to post a meme with more than 4 tags`, async () => {
    const res = await request(app.getHttpServer())
      .post('/memes')
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken)
      .send(mockData.newMemeMoreTags);
    return expect(res.status).toEqual(400);
  });
  it(`/POST unauthenticated User devGitego should be not be able to post a meme`, async () => {
    const res = await request(app.getHttpServer())
      .post('/memes')
      .send(mockData.newMeme);
    return expect(res.status).toEqual(401);
  });
  it(`/POST Authenticated User gitegob7 should be able to post a meme`, async () => {
    const res = await request(app.getHttpServer())
      .post('/memes')
      .set('Authorization', 'Bearer ' + mockData.gitegob7Token)
      .send(mockData.newMeme);
    mockData.gitegob7MemeId = res.body.data.id;
    return expect(res.status).toEqual(201);
  });
  it(`/GET any User should be able to get all memes`, async () => {
    const res = await request(app.getHttpServer()).get('/memes?page=0');
    return expect(res.status).toEqual(200);
  });
  it(`/GET any User should be able to get all memes without specifying a page`, async () => {
    const res = await request(app.getHttpServer()).get('/memes');
    return expect(res.status).toEqual(400);
  });
  it(`/GET any User should be able to get one meme`, async () => {
    const res = await request(app.getHttpServer()).get(
      `/memes/${mockData.gitegob7MemeId}`,
    );
    return expect(res.status).toEqual(200);
  });
  it(`/GET any User should be able to get a meme with an invalid uuid`, async () => {
    const res = await request(app.getHttpServer()).get(
      `/memes/${mockData.gitegob7MemeId}d`,
    );
    return expect(res.status).toEqual(400);
  });
  it(`/GET authenticated User should be able to get all featured memes`, async () => {
    const res = await request(app.getHttpServer())
      .get('/memes/featured')
      .set('Authorization', 'Bearer ' + mockData.gitegob7Token);
    return expect(res.status).toEqual(200);
  });
  it(`/GET any User should be able to get all memes by one user`, async () => {
    const res = await request(app.getHttpServer()).get(
      `/memes/user/${mockData.devGitegoId}?page=0`,
    );
    return expect(res.status).toEqual(200);
  });
  it(`/GET any User should be able to search for a meme through all memes`, async () => {
    const res = await request(app.getHttpServer()).get(
      `/memes/search/?tag=test`,
    );
    return expect(res.status).toEqual(200);
  });
  it(`/GET any User should be able to search for a meme through a user's memes`, async () => {
    const res = await request(app.getHttpServer()).get(
      `/memes/search/${mockData.devGitegoId}/?tag=test`,
    );
    return expect(res.status).toEqual(200);
  });
  it(`/GET authenticated User should be able to delete a meme`, async () => {
    const res = await request(app.getHttpServer())
      .get(`/memes/${mockData.gitegob7MemeId}`)
      .set('Authorization', 'Bearer ' + mockData.gitegob7Token);
    return expect(res.status).toEqual(200);
  });
});
