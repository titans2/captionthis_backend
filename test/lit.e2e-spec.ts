import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { Connection, getConnection, getRepository } from 'typeorm';
import { AppModule } from '../src/app.module';
import mockData, { createSeedUsers } from './utils/mockData';
import { User } from '../src/auth/entities/auth.entity';

describe('Memes (e2e)', () => {
  let app: INestApplication;
  let connection: Connection;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    connection = getConnection();
    await connection.synchronize(true);
    // SEED USERS
    const userRepo = getRepository(User);
    await userRepo.save(createSeedUsers());
    const res = await request(app.getHttpServer())
      .post('/memes')
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken)
      .send(mockData.newMeme);
    mockData.devGitegoMemeId = res.body.data.id;
    mockData.devGitegoId = res.body.data.user;
    const res2 = await request(app.getHttpServer())
      .post('/memes')
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken)
      .send(mockData.newMeme);
    mockData.devGitegoMemeId = res2.body.data.id;
    mockData.devGitegoId = res2.body.data.user;
    const res3 = await request(app.getHttpServer())
      .post(`/captions/meme/${mockData.devGitegoMemeId}`)
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken)
      .send(mockData.newCaption);
    mockData.devGitegoCaptionId = res3.body.data.id;
    return expect(res3.status).toEqual(201);
  });
  afterAll(async () => {
    await connection.synchronize(true);
    await app.close();
  });
  it(`/POST User should be able to lit a caption`, async () => {
    const res = await request(app.getHttpServer())
      .put(`/lits/caption/${mockData.devGitegoCaptionId}`)
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken);
    expect(res.status).toEqual(200);
    expect(res.body.data).toHaveProperty('litAdded');
    return;
  });
  it(`/POST User should be able to unlit a caption`, async () => {
    const res = await request(app.getHttpServer())
      .put(`/lits/caption/${mockData.devGitegoCaptionId}`)
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken);
    expect(res.status).toEqual(200);
    expect(res.body.data).toHaveProperty('litRemoved');
    return;
  });
});
