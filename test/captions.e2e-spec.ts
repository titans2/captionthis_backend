import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { Connection, getConnection, getRepository } from 'typeorm';
import { AppModule } from '../src/app.module';
import mockData, { createSeedUsers } from './utils/mockData';
import { User } from '../src/auth/entities/auth.entity';

describe('Memes (e2e)', () => {
  let app: INestApplication;
  let connection: Connection;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    connection = getConnection();
    await connection.synchronize(true);
    // SEED USERS
    const userRepo = getRepository(User);
    await userRepo.save(createSeedUsers());
    const res = await request(app.getHttpServer())
      .post('/memes')
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken)
      .send(mockData.newMeme);
    mockData.devGitegoMemeId = res.body.data.id;
    mockData.devGitegoId = res.body.data.user;
    const res2 = await request(app.getHttpServer())
      .post('/memes')
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken)
      .send(mockData.newMeme);
    mockData.devGitegoMemeId = res2.body.data.id;
    mockData.devGitegoId = res2.body.data.user;
  });
  afterAll(async () => {
    await connection.synchronize(true);
    await app.close();
  });
  it(`/POST authenticated User should be able to caption a meme`, async () => {
    const res = await request(app.getHttpServer())
      .post(`/captions/meme/${mockData.devGitegoMemeId}`)
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken)
      .send(mockData.newCaption);
    mockData.devGitegoCaptionId = res.body.data.id;
    return expect(res.status).toEqual(201);
  });
  it(`/POST unauthenticated User should not be able to caption a meme`, async () => {
    const res = await request(app.getHttpServer())
      .post(`/captions/meme/${mockData.devGitegoMemeId}`)
      .send(mockData.newCaption);
    return expect(res.status).toEqual(401);
  });
  it(`/GET any User should be able to get all captions on one meme`, async () => {
    const res = await request(app.getHttpServer()).get(
      `/captions/meme/${mockData.devGitegoMemeId}?page=0`,
    );
    return expect(res.status).toEqual(200);
  });
  it(`/GET any User should be able to get all captions by a user`, async () => {
    const res = await request(app.getHttpServer())
      .get(`/captions/user/${mockData.devGitegoId}?page=0`)
      .set('Authorization', 'Bearer ' + mockData.gitegob7Token);
    return expect(res.status).toEqual(200);
  });
  it(`/GET User should not be able to delete another's caption`, async () => {
    const res = await request(app.getHttpServer())
      .delete(`/captions/${mockData.devGitegoCaptionId}`)
      .set('Authorization', 'Bearer ' + mockData.gitegob7Token);
    return expect(res.status).toEqual(404);
  });
  it(`/GET User should be able to delete their caption`, async () => {
    const res = await request(app.getHttpServer())
      .delete(`/captions/${mockData.devGitegoCaptionId}`)
      .set('Authorization', 'Bearer ' + mockData.devGitegoToken);
    return expect(res.status).toEqual(200);
  });
});
