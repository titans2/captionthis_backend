import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { db, global } from './src/__shared__/config/env.config';

const ormconfig: TypeOrmModuleOptions = {
  ...db,
  type: 'postgres',
  entities: [
    __dirname + '/src/**/**/*.entity.js',
    __dirname + '/src/**/**/*.entity.ts',
  ],
  synchronize: true,
  migrations: ['dist/src/db/migrations/*.js'],
  cli: {
    migrationsDir: 'src/db/migrations',
  },
  ssl:
    global.environment === 'production'
      ? {
          rejectUnauthorized: false,
        }
      : false,
};

export default ormconfig;
