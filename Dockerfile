FROM node:14.16.0-alpine3.10

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install

COPY . .

CMD yarn start