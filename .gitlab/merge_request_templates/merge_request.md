#### What does this PR do?

-

#### Description of Task to be completed?

-

#### How should this be manually tested?

---

> **Steps to manually test the functionalities implemented in this PR**

- After cloning the [repo](https://gitlab.com/titans2/captionthis_backend.git), and `cd [the project name]`
- Check out this branch by `git checkout [the branch for this pr]`
- Run `npm install` to install the project dependencies
- Run `npm run migrate` to migrate new tables to the database
- Run `npm run start:dev` to start the server in the development mode

#### Any background context you want to provide?

N/A

#### What are the relevant pivotal tracker stories?

[#CAP-](https://gitego-brian.atlassian.net/browse/CAP-)

#### Screenshots (if appropriate)

N/A

#### Questions:

N/A
