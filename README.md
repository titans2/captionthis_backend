# captionThis
It's simple. You submit your funny or odd pics that could use a good caption.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Authors

- [NIYODUSENGA Clement](https://gitlab.com/niyodusengaclement)
- [Brian Gitego](https://gitlab.com/gitego-brian)
- [NYAGATARE James](https://gitlab.com/nyagatarejames)

